In new homedir without git:
```
curl https://gitlab.com/chris.perelstein/home/-/archive/home/home-home.tar.gz | tar xz --strip-components 1
```

In new homedir with git:
```
git clone --branch home git@gitlab.com:chris.perelstein/home.git ~
```
